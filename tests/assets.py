import unittest

from os.path import isfile, join, dirname

from pmenu.conf import Conf
from pmenu.request import DataBase
from pmenu.messages import error


class AssetsTest(unittest.TestCase):

    def test_is_asset_in_database(self):
        conf = Conf()
        db = DataBase(conf)

        all_apps = db.get_all_apps()
        all_icons = {}

        assets_path = join(
            dirname(dirname(__file__)), 'static', 'assets', 'apps'
        )

        for app in all_apps:
            if app['default_icon_path']:
                all_icons[join(
                    assets_path,
                    app['default_icon_path']
                )] = app
            if app['mini_icon_path']:
                all_icons[join(
                    assets_path,
                    app['mini_icon_path']
                )] = app
            if app['super_icon_path']:
                all_icons[join(
                    assets_path,
                    app['super_icon_path']
                )] = app
            if app['maxi_icon_path']:
                all_icons[join(
                    assets_path,
                    app['maxi_icon_path']
                )] = app
            if app['prof_icon_path']:
                all_icons[join(
                    assets_path,
                    app['prof_icon_path']
                )] = app

        for icon_path in all_icons:
            icon_path_exist = isfile(icon_path)
            if not icon_path_exist:
                error(f'ce chemin n\'exite pas : "{icon_path}" pour {all_icons[icon_path]["name"]}')  # noqa: E501
            self.assertTrue(icon_path_exist)
