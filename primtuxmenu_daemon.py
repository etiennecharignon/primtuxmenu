import sys

import pyinotify

from pmenu.conf import Conf
from pmenu.request import DataBase
from pmenu.requests.apps import DbApps
from pmenu.package import (
    AptList,
    all_apt_installed, desktops_path_for_app
)

import secrets

from flask import Flask
from flask_socketio import SocketIO, emit

app = Flask(__name__)

app.secret_key = secrets.token_urlsafe(32)
socketio = SocketIO(
    app,
    cors_allowed_origins="*",
    logger=True,
    engineio_logger=True
)


class PrimtuxMenuEventHandler(pyinotify.ProcessEvent):
    def __init__(self):
        conf = Conf()
        self.db = DataBase(conf)
        self.db_apps = DbApps(self.db)
        self.apt_list = AptList(conf.apt_list_cache)

    def my_init(self, file_object=sys.stdout):
        pass

    def process_IN_CREATE(self, event):
        self._update_apps()

    def process_IN_CLOSE_WRITE(self, event):
        self._update_apps()

    def process_IN_DELETE(self, event):
        self._update_apps()

    def _update_apps(self):
        all_apps = list(self.db.get_all_apps())

        new_list_app = all_apt_installed()
        apps_diff = self.apt_list.diff(new_list_app)

        for app in all_apps:
            if app['apt_name'] not in apps_diff.new_apps:
                continue
            list_of_desktop_files = desktops_path_for_app(app['apt_name'])
            self.db_apps.update({
                'is_available': 1,
                'is_apt_installed': 1,
                'desktop_path': list_of_desktop_files[0],
                'id': app['id']
            })

        for app in all_apps:
            if app['apt_name'] not in apps_diff.removed_apps:
                continue
            self.db_apps.update({
                'is_available': 0,
                'is_apt_installed': 0,
                'id': app['id']
            })
        self.db.db.commit()
        self.apt_list.write(new_list_app)
        emit('watch')


@socketio.on('message')
def message(data):
    if 'first' not in data:
        return
    wm = pyinotify.WatchManager()
    notifier = pyinotify.Notifier(
        wm,
        PrimtuxMenuEventHandler()
    )

    watch_files = [
        '/var/log/dpkg.log',
        '/var/log/apt/history.log',
    ]

    for d in watch_files:
        wm.add_watch(
            d,
            pyinotify.IN_CREATE
            | pyinotify.IN_DELETE
            | pyinotify.IN_CLOSE_WRITE
        )

    notifier.loop()


@socketio.on('connect')
def connect():
    emit('start')


if __name__ == "__main__":
    socketio.run(app, port=7000)
