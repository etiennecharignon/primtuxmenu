/* session mini */

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) 
VALUES (
    'mini',
    'Français',
    'french',
    'francais.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) 
VALUES (
    'mini',
    'Mathématiques',
    'mathematics',
    'maths-cycle-1.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'mini',
    'Activités artistiques',
    'artistic',
    'activites-artistiques.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'mini',
    'Explorer le monde',
    'world',
    'explorer-le-monde.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'mini',
    'Jeux',
    'games',
    'jeux.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'mini',
    'Explorer le monde',
    "Le temps et l'espace",
    'world',
    'le-temps-et-l-espace.svg',
    0,
    0
);
INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'mini',
    'Explorer le monde',
    'Sciences',
    'world',
    'sciences-cycle 1.svg',
    0,
    0
);

/* session super */

INSERT INTO levels (
        session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Français',
    'french',
    'francais.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
 )
 VALUES (
    'super',
    'Français',
    "Lecture-sons",
    'french',
    'lecture-sons.svg',
    0,
    0
 );

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Français',
    "Lecture-compréhension",
    'french',
    'lecture-comprehension.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Français',
    "Grammaire",
    'french',
    'grammaire.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'super',
    'Français',
    "Conjugaison",
    'french',
    'conjugaison.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'super',
    'Français',
    "Orthographe",
    'french',
    'orthographe.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'super',
    'Français',
    "Lexique",
    'french',
    'lexique.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'super',
    'Mathématiques',
    'mathematics',
    'maths-cycles-2-et-3.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Mathématiques',
    "Numération",
    'mathematics',
    'numeration.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Mathématiques',
    "Calcul",
    'mathematics',
    'calcul.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Mathématiques',
    "Grandeur et mesure",
    'mathematics',
    'grandeur-et-mesure.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Mathématiques',
    "Géométrie",
    'mathematics',
    'geometrie.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Langue vivante',
    'languages',
    'langue-vivante.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Histoire géographie',
    'world',
    'histoire-geographie.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Sciences',
    'sciences',
    'sciences.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Enseignements artistiques',
    'artistic',
    'activites-artistiques.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Enseignement moral et civique',
    'emc',
    'emc.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Jeux',
    'games',
    'jeux.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'super',
    'Accessoires',
    'accessories',
    'accessoires.svg',
    0,
    0
);

/* session maxi */

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Français',
    'french',
    'francais.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Français',
    "Lecture-compréhension",
    'french',
    'lecture-comprehension.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Français',
    "Grammaire",
    'french',
    'grammaire.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Français',
    "Conjugaison",
    'french',
    'conjugaison.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Français',
    "Orthographe",
    'french',
    'orthographe.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Français',
    "Lexique",
    'french',
    'lexique.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Mathématiques',
    'mathematics',
    'maths-cycles-2-et-3.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Mathématiques',
    "Numération",
    'mathematics',
    'numeration.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Mathématiques',
    "Calcul",
    'mathematics',
    'calcul.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Mathématiques',
    "Grandeur et mesure",
    'mathematics',
    'grandeur-et-mesure.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    name_level_two,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Mathématiques',
    "Géométrie",
    'mathematics',
    'geometrie.svg',
    0,
    0
);


INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Langue vivante',
    'languages',
    'langue-vivante.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Histoire géographie',
    'world',
    'histoire-geographie.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Sciences',
    'sciences',
    'sciences.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'maxi',
    'Enseignements artistiques',
    'artistic',
    'activites-artistiques.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Enseignement moral et civique',
    'emc',
    'emc.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'maxi',
    'Jeux',
    'games',
    'jeux.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
) VALUES (
    'maxi',
    'Accessoires',
    'accessories',
    'accessoires.svg',
    0,
    0
);

/* session prof */

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'prof',
    'Bureautique',
    'french',
    'bureautique.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'prof',
    'Multimédia',
    'world',
    'multimedia.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'prof',
    'Graphisme',
    'artistic',
    'graphisme.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    icon_path,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'prof',
    'Outils du professeur',
    'games',
    'outils-du-professeur.svg',
    0,
    0
);

INSERT INTO levels (
    session,
    name_level_one,
    color,
    nb_installed_apps,
    nb_not_installed_apps
)
VALUES (
    'prof',
    'Divers',
    'mathematics',
    0,
    0
);
