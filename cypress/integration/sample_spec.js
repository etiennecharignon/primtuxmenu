function screen(_session, suffix='', callback=null) {
  cy.viewport(1280, 768)
  let url = 'http://127.0.0.1:5500/' + _session
  cy.visit(url)
  if (callback)
    callback()
  cy.screenshot({
    name: 'session_' + _session + suffix, 
    clip: { x: 0, y: 0, width: 1280, height: 768 } 
  })
}

function open_menu(_session) {
    screen(
        _session,
        '_open_menu',
        () => {
            cy.get('#select-menu__button').click()
            cy.get('body').focused()
        }
    )
}

describe('Screenshots', () => {

  it('session mini', () => {
    screen('mini')
  })

  it('session super', () => {
    screen('super')
  })

  it('session maxi', () => {
    screen('maxi')
  })

  it('session prof', () => {
    screen('prof')
  })

  it("session prof : ouverture du menu", () => {
    open_menu('prof')
  })

  it("session prof : ouverture de la description d'une app", () => {
    screen(
      'prof',
      '_open_app_description',
      () => {
        cy.get('li.app__element:nth-child(3)').click()
        cy.get(
          '#c-modal',
          {timeout: 10000 }
        ).should('be.visible')
      }
    )
  })

  it("session prof : ouverture de création d'une app", () => {
    screen(
      'prof',
      '_open_app_creator',
      () => {
        cy.get('#launch-app-creation').click()
        cy.get(
          '#c-modal',
          {timeout: 10000 }
        ).should('be.visible')
      }
    )
  })

  it("session prof : ouverture de la modification d'une app", () => {
    screen(
      'prof',
      '_open_app_update',
      () => {
        cy.get('#edit-mode-checkbox').click()
        cy.get('li.app__element:nth-child(3)').click()
        cy.get(
          '#c-modal',
          {timeout: 10000 }
        ).should('be.visible')
      }
    )
    cy.get('#edit-mode-checkbox').click()
  })


  it("session prof : sidebar d'installation", () => {
    screen(
      'prof',
      '_installation_sidebar',
      () => { 
        cy.get('#installation-preview').click()
      }
    )
  })
})
