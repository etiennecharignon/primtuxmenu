DELTA = 2


def pagination(current, last):
    if last <= 1:
        return

    _range = []
    range_with_dots = []
    index_minus_one = None

    for i in range(1, last + 1):
        if (
            i == 1 or i == last
            or i >= (current - DELTA)
            and i < (current + DELTA + 1)
        ):
            _range.append(i)

    for i in _range:
        if index_minus_one:
            if (i - index_minus_one) == 2:
                range_with_dots.append(index_minus_one + 1)
            elif i - index_minus_one != 1:
                range_with_dots.append('...')
        range_with_dots.append(i)
        index_minus_one = i

    return range_with_dots
