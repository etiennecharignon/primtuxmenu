

def get_sessions():
    return {
        'mini': {
            'type': 'mini',
            'name': 'Jerry',
            'cycle': 'cycle 1 : maternelle',
            'menu': []
        },
        'super': {
            'type': 'super',
            'name': 'Koda',
            'cycle': 'cycle 2 : CP, CE1 et CE2',
            'menu': []
        },
        'maxi': {
            'type': 'maxi',
            'name': 'Léon',
            'cycle': 'cycle 3 : CM1 et CM2',
            'menu': []
        },
        'prof': {
            'type': 'prof',
            'name': 'Poe',
            'cycle': """accès à l'ensemble des 3 cycles""",
            'menu': []
        }
    }


def add_sub_menu(menu_items, menu, name_level_one):
    for item in menu_items:
        if item['name_level_one'] != name_level_one:
            continue
        menu_item = {}
        menu_item['nb_apps'] = item['nb_apps']
        menu_item['name'] = item['name_level_two']
        menu_item['icon_path'] = item['icon_path']
        menu_item['color'] = item['color']
        if 'menu' not in menu_item:
            menu_item['menu'] = []
        menu.append(menu_item)


def get_all_menus(levels):
    menu_items = []
    sub_menu_items = []

    for cat in levels:
        if cat['name_level_two']:
            sub_menu_items.append(cat)
            continue
        menu_items.append(cat)

    _sessions = get_sessions()
    for item in menu_items:
        menu = _sessions[item['session']]['menu']
        menu_item = {}
        menu_item['nb_apps'] = item['nb_apps']
        menu_item['name'] = item['name_level_one']
        menu_item['icon_path'] = item['icon_path']
        menu_item['color'] = item['color']
        if 'menu' not in menu_item:
            menu_item['menu'] = []
        add_sub_menu(
            sub_menu_items,
            menu_item['menu'],
            item['name_level_one']
        )
        menu.append(menu_item)
    return _sessions
