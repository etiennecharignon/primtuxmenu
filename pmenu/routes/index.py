from os.path import join
from pathlib import Path
import math

from flask import Blueprint, send_from_directory, render_template, request

from pmenu.routes.lib import init
from pmenu.menu import get_all_menus
from pmenu.pagination import pagination
from pmenu.conf import Conf

index_page = Blueprint(
    'index_page',
    __name__,
    template_folder='templates'
)


@index_page.route('/favicon.ico')
async def favicon():
    return send_from_directory(
        join(Path(index_page.root_path).parent.parent, 'static'),
        'favicon.ico', mimetype='image/vnd.microsoft.icon')


@index_page.route('/')
@index_page.route('/<session_name>')
async def main(session_name=None):
    preferences = init.db_prefs.get()
    if preferences:
        filter_by = preferences['filter_by']

    session_selected = 'prof'

    if not session_name:
        return render_template(
            'index.html',
            session={'type': 'index'}
        )

    if session_name == 'prof' and 'session_selected' in preferences.keys():
        session_selected = preferences['session_selected']
        if not session_selected:
            session_selected = 'prof'
    if session_name != 'prof':
        session_selected = session_name
        filter_by = 0

    page_index = 0
    level_name = None
    sub_level_name = None
    if 'page_index' in request.args:
        page_index = int(request.args['page_index'])
    if 'level_name' in request.args:
        level_name = request.args['level_name']
    if 'sub_level_name' in request.args:
        sub_level_name = request.args['sub_level_name']

    nb_apps = init.db.count_apps_from_level(
        session_selected,
        filter_by,
        level_name,
        sub_level_name
    )

    # TODO : à lancer uniquement la première fois
    # qu'on se met en mode édition (ne semble pas utile avant)
    if 'sessions' not in init.flask_session:
        all_levels = init.db.get_levels().fetchall()
        init.flask_session['sessions'] = get_all_menus(all_levels)

    levels = get_all_menus(init.db.get_levels(
        session_selected,
        filter_by
    ).fetchall())[session_selected]['menu']
    session = init.sessions[session_name]

    if nb_apps == 0:
        return render_template(
            'session.html',
            page_index=page_index,
            session=session,
            session_name=session_name,
            preferences=preferences,
            menu=levels,
            nb_apps=0,
            level_name=level_name,
            sub_level_name=sub_level_name
        )
    results_by_page = init.conf.results_by_page
    nb_pages = math.ceil(nb_apps / init.conf.results_by_page)

    apps = init.db.get_page_from_level(
        page_index * init.conf.results_by_page,
        results_by_page,
        session_selected,
        filter_by,
        level_name,
        sub_level_name
    ).fetchall()

    return render_template(
        'session.html',
        page_index=page_index,
        session=session,
        session_name=session_name,
        session_selected=session_selected,
        preferences=preferences,
        menu=levels,
        apps=apps,
        nb_apps=nb_apps,
        level_name=level_name,
        sub_level_name=sub_level_name,
        pagination=pagination(page_index, nb_pages)
    )


@index_page.route('/external_url/<id>')
async def external_url(id):
    external_url = init.db.get_external_url_by_id(id)
    return render_template(
        'external_url.html',
        external_url=external_url
    )


@index_page.route('/logout')
async def logout():
    conf = Conf()
    conf.last_app = "xfce4-session-logout --logout"
    conf.update()
    return 'launched'
