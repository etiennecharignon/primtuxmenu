import shutil
from os.path import join, dirname
from pathlib import Path

from flask import Blueprint, render_template, request

from pmenu.init import UUIdGen
from pmenu.routes.lib import init, _update_apps


admin_page = Blueprint(
    'admin_page',
    __name__,
    template_folder='templates'
)
root_path = Path(dirname(__file__)).parent.parent
app_details_keys = ['description', 'link', 'detail_id']
is_bool = ['in_mini', 'in_super', 'in_maxi', 'in_prof', 'is_available']


def format_img_path(app_list):
    icon_paths = [
        'default_icon_path',
        'mini_icon_path',
        'super_icon_path',
        'maxi_icon_path'
    ]
    for img in icon_paths:
        if img not in app_list:
            continue
        icon_name = app_list[img]
        if icon_name == '':
            del app_list[img]
            continue
        tmp_path = join(root_path, 'tmp', icon_name)
        final_path = join(root_path, 'static/assets/apps', icon_name)
        shutil.move(tmp_path, final_path)
        app_list[img] = '/assets/apps/%s' % icon_name
    return app_list


@admin_page.route('/create-app')
async def _create_app():
    session_name = request.args['session_name']
    modal_deco = render_template(
        'edition/decoration_app.html',
        update=False
    )
    modal_body = render_template(
        'edition/edit_app.html',
        uuid=UUIdGen(),
        menus=init.flask_session['sessions'],
        licenses=init.conf.licenses,
        app={},
        update=False
    )
    modal_footer = render_template(
        'edition/action_app.html',
        session_name=session_name,
        update=False
    )
    return {
        'modal_deco': modal_deco,
        'modal_body': modal_body,
        'modal_footer': modal_footer
    }


@admin_page.route('/insert-app', methods=['POST'])
async def _insert_app():
    req = request.get_json()
    session_name = req.pop('session_name')
    session_selected = req.pop('session_selected')
    filter_by = req.pop('filter_by')

    app_list = {
        key: req[key]
        for key in req
        if key not in app_details_keys
    }

    for key in is_bool:
        if key not in req:
            continue

    app_details_list = {
        key: req[key]
        for key in req
        if key in app_details_keys + ['id']
    }

    init.db.insert_app(format_img_path(app_list))
    init.db.insert_app_details(app_details_list)
    init.db.db.commit()
    return await _update_apps(session_name, session_selected, filter_by)


@admin_page.route('/update-app', methods=['POST'])
async def _update_app():
    req = request.get_json()
    session_name = req.pop('session_name')
    session_selected = req.pop('session_selected')
    filter_by = req.pop('filter_by')

    print(req)
    app_list = {key: req[key] for key in req if key not in app_details_keys}

    for key in is_bool:
        if key not in req:
            continue
    app_details_list = {
        key: req[key]
        for key in req
        if key in app_details_keys + ['id']
    }

    init.db.insert_app(format_img_path(app_list))
    init.db.insert_app_details(app_details_list)
    init.db.db.commit()
    return await _update_apps(session_name, session_selected, filter_by)


@admin_page.route('/delete-app', methods=['POST'])
async def _delete_app():
    req = request.get_json()
    init.db.delete_app(
        req['id'],
        req['app_key'] if 'app_key' in req else None
    )
    return await _update_apps(
        req['session_name'],
        req['session_selected'],
        int(req['filter_by'])
    )


@admin_page.route('/admin/edition-mode', methods=['POST'])
async def _edition_mode():
    req = request.get_json()
    init.db_prefs.update({'edition': req['edit']})
    init.db.db.commit()
    return {'result': True}


@admin_page.route('/upload-icon', methods=['POST'])
async def upload_icon():
    uploads = request.files
    forms = request.form
    _file = uploads['img']
    Path(
        join(root_path, 'tmp')
    ).mkdir(parents=True, exist_ok=True)
    _file.save(join(root_path, 'tmp', forms['img_name']))
    return {'result': True}


@admin_page.route('/flush-tmp-files', methods=['POST'])
async def flush_tmp_files():
    shutil.rmtree(join(root_path, 'tmp'))
    Path(
        join(root_path, 'tmp')
    ).mkdir(parents=True, exist_ok=True)
    return {'result': True}
