import re

import sqlite3

has_pygments = True
try:
    from pygments import highlight
    from pygments.lexers import SqlLexer
    from pygments.formatters import TerminalTrueColorFormatter
except ModuleNotFoundError:
    has_pygments = False


def print_pygmentyze(value):
    value = f' {value}'.replace('\n', ' ').replace("  ", "")
    if not has_pygments:
        print(value)
        return
    print(highlight(
        f'sql : {value}',
        SqlLexer(),
        TerminalTrueColorFormatter(style='native')
    ))


def bool_to_integer(value):
    if value:
        return '1'
    return '0'


class DataBase:
    def __init__(self, conf):
        self.debug = conf.DEBUG
        self.db = sqlite3.connect(
            conf.db.path,
            check_same_thread=False,
            isolation_level=None,
            timeout=0.01
        )
        if self.debug:
            self.db.set_trace_callback(print_pygmentyze)
        self.db.row_factory = sqlite3.Row

    def _where_like(self, value=None, _and=False):
        _where = ''
        if value:
            _where = "WHERE k.text LIKE '" + value + "%'"
        if _and:
            _where = 'WHERE %s' % _where
        else:
            _where = ' AND %s' % _where
        return _where

    def exec(self, request, commit=False):
        if self.debug:
            request = re.sub(r'\s+', ' ', request).strip()
        result = self.db.execute(request)
        if commit:
            self.db.commit()
        return result

    def _format_type(self, value):
        _type = type(value)
        if _type is bool:
            return bool_to_integer(value)
        return '"%s"' % value

    def _insert(self, table_name, _list):
        request = 'INSERT INTO %s (' % table_name
        for count, value in enumerate(_list.keys()):
            if count == 0:
                request += value
                continue
            request += ', %s' % value
        request += ') VALUES('
        for count, value in enumerate(_list.values()):
            if count == 0:
                request += self._format_type(value)
                continue
            request += ', %s' % self._format_type(value)
        request += ');'
        return self.exec(request)

    def _update(self, table_name, _list):
        request = 'UPDATE %s SET ' % table_name
        _id = _list.pop('id')
        for count, value in enumerate(_list):
            if count == 0:
                request += '%s = %s' % (
                    value,  self._format_type(_list[value])
                )
                continue
            request += ', %s = %s' % (
                value,  self._format_type(_list[value])
            )
        request += " WHERE id=%s;" % _id
        return self.exec(request)

    def insert_or_update(self, table_name, _list):
        if _list == []:
            return
        if 'id' in _list.keys() and _list['id'] != '':
            return self._update(table_name, _list)
        else:
            return self._insert(table_name, _list)

    def delete(self, table_name, _id, index=None):
        if not index:
            index = 'id'
        return self.exec(
            '''
            DELETE FROM %s
            WHERE %s = %s;
            ''' % (table_name, index, _id)
        )

    def get(self, _id, session=None):
        paths = ''
        if session:
            paths = f'''
                a.{session}_icon_path AS icon_path,
                a.{session}_path AS PATH, a.default_path,
            '''
        return self.exec(
            f'''
            SELECT a.id, a.name, a.apt_name, a.generic,
            {paths}
            a.is_available,
            a.mini_path, a.super_path,
            a.maxi_path, a.prof_path,
            a.in_mini,
            a.in_maxi,
            a.in_super,
            a.in_prof,
            a.license,
            a.is_web_app,
            a.is_external_url,

            a.mini_icon_path,
            a.super_icon_path,
            a.maxi_icon_path,
            a.default_icon_path,

            l.session,
            l.name_level_one,
            l.name_level_two,

            d.id AS detail_id,
            d.description, d.link
            FROM apps AS a
            LEFT JOIN app_in_level AS a_l ON a.id = a_l.app_key
            LEFT JOIN levels AS l ON a_l.level_key = l.id
            LEFT JOIN apps_detail AS d ON d.app_key = a.id
            WHERE a.id = "{_id}";
            ''',
            True
        ).fetchone()

    def get_all_apps(self):
        return self.exec(
            '''
            SELECT * FROM apps ORDER BY name
            '''
        )

    def _clause_is_available(self, filter_by=0):
        if filter_by == 0:
            return ' is_available = 1'
        if filter_by == 1:
            return ' is_available = 0'
        return ''

    def count_apps_from_level(
        self,
        session,
        filter_by=0,
        level_one=None,
        level_two=None
    ):
        _where = 'WHERE in_%s = 1' % session
        _is_available = self._clause_is_available(filter_by)
        if _is_available:
            _where += ' AND %s' % _is_available

        if level_one:
            _where += f'''
            AND l.name_level_one = "{level_one}"
            AND l.session = "{session}"
            '''
        if level_two:
            _where += f' AND l.name_level_two = "{level_two}"'
        return self.exec(
            f'''
            SELECT count(DISTINCT a.id) FROM apps AS a
            LEFT JOIN app_in_level AS a_l ON a.id = a_l.app_key
            LEFT JOIN levels AS l ON a_l.level_key = l.id
            {_where};
            '''
        ).fetchone()[0]

    def get_page_from_level(
        self,
        offset,
        nb,
        session,
        filter_by=0,
        level_one=None,
        level_two=None
    ):
        _where = 'WHERE in_%s = 1' % session
        _is_available = self._clause_is_available(filter_by)
        if _is_available:
            _where += ' AND %s' % _is_available

        if level_one:
            _where += f'''
            AND l.name_level_one = "{level_one}"
            AND l.session = "{session}"
            '''
        if level_two:
            _where += f' AND l.name_level_two = "{level_two}"'
        return self.exec(
            f'''
            SELECT DISTINCT a.id, name, apt_name, generic,
            {session}_icon_path AS icon_path, default_icon_path,
            {session}_path AS path, default_path,
            is_available, is_web_app, is_external_url
            FROM apps AS a
            LEFT JOIN app_in_level AS a_l ON a.id = a_l.app_key
            LEFT JOIN levels AS l ON a_l.level_key = l.id
            {_where}
            ORDER BY name
            LIMIT {offset}, {nb};
            '''
        )

    def get_levels(
        self,
        session=None,
        filter_by=0
    ):
        nb_installed = '(nb_installed_apps + nb_not_installed_apps) AS nb_apps'
        if filter_by == 0:
            nb_installed = 'nb_installed_apps AS nb_apps'
        if filter_by == 1:
            nb_installed = 'nb_not_installed_apps AS nb_apps'
        _where = ''
        if session:
            _where = 'WHERE session = "%s"' % session
        return self.exec(
            '''
            SELECT id,
            session,
            name_level_one,
            name_level_two,
            color,
            icon_path,
            %s
            FROM levels %s
            ''' % (nb_installed, _where),
            True
        )

    def app_exist(self, name, sessions, session):
        result = self.exec(
            '''
            SELECT %s, %s, %s, %s
            FROM apps WHERE name = "%s";
            ''' % (
                'in_%s' % sessions[0],
                'in_%s' % sessions[1],
                'in_%s' % sessions[2],
                session,
                name
            )
        ).fetchone()
        if not result:
            return None
        return {
            sessions[0]: result[0],
            sessions[1]: result[1],
            sessions[2]: result[2],
            session: result[3]
        }

    def add_app(
        self,
        sessions,
        app_name,
        name,
        icon_path,
        cmd,
        generic,
        session
    ):
        return self.exec(
            '''
            INSERT INTO apps
            (name, apt_name, generic, icon_path, path, %s, %s, %s, %s)
            VALUES ("%s", "%s", "%s", "%s", '%s', 0, 0, 0, 1);
            ''' % (
                'in_%s' % sessions[0],
                'in_%s' % sessions[1],
                'in_%s' % sessions[2],
                session,
                name,
                app_name,
                generic,
                icon_path,
                cmd
            ),
            True
        ).lastrowid

    def add_keyword(self, keyword, id_apps):
        keywords_cursor = self.exec(
            '''
            SELECT k.id FROM keywords AS k
            WHERE k.text = "%s";
            ''' % keyword
        ).fetchone()
        if keywords_cursor:
            keywords_id = keywords_cursor[0]
        else:
            keywords_id = self.exec(
                '''
                INSERT INTO keywords(text) VALUES("%s");
                ''' % keyword
            ).lastrowid
        self.exec(
            '''
            INSERT INTO contains(id_apps, id_keywords)
            VALUES (%s, %s);
            ''' % (id_apps, keywords_id)
        )

    def get_keywords(self, value=None):
        return self.exec(
            '''
            SELECT k.text, count(k.id) FROM keywords AS k
            INNER JOIN contains AS c ON k.id = c.id_keywords
            %s
            GROUP BY k.id
            ORDER BY count(k.id) DESC
            LIMIT 20;
            ''' % self._where_like(value)
        )

    def update_apt_app(self, _id):
        return self.exec(
            '''
            UPDATE apps SET is_apt_installed = 1
            WHERE id = %s
            ''' % _id
        )

    def update_installed_app(self, apt_name):
        return self.exec(
            '''
            UPDATE apps SET is_available = 1
            WHERE apt_name = '%s'
            ''' % apt_name
        )

    def count_not_installed(self, apt_name):
        return self.exec(
            '''
            SELECT count(id) FROM apps WHERE apt_name = '%s'
            AND is_available = 0;
            ''' % apt_name
        ).fetchone()[0]

    def get_external_url_by_id(self, id):
        return self.exec(
            "select default_path from apps where id = '%s'" % id
        ).fetchone()[0]

    def update_level(
        self,
        _id,
        nb_installed_apps,
        nb_not_installed_apps
    ):
        return self.exec(
            '''
            UPDATE levels
            SET nb_installed_apps = %s,
            nb_not_installed_apps = %s
            WHERE id = %s
            ''' % (nb_installed_apps, nb_not_installed_apps, _id)
        )

    def insert_app(
        self,
        _list
    ):
        if len(_list) < 2:
            return
        return self.insert_or_update('apps', _list)

    def insert_app_details(
        self,
        _list
    ):
        if 'id' in _list:
            _list['app_key'] = _list.pop('id')
        if len(_list) < 2:
            return
        if 'detail_id' in _list:
            _list['id'] = _list.pop('detail_id')
            if len(_list) < 3:
                return
        return self.insert_or_update('apps_detail', _list)

    def delete_app(self, _id, app_key):
        self.delete('apps', _id)
        if app_key:
            self.delete('apps_detail', _id, app_key)
        self.db.commit()
