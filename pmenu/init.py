from uuid import uuid4

from flask import session as flask_session

from pmenu.conf import Conf
from pmenu.menu import get_sessions
from pmenu.request import DataBase
from pmenu.requests.preferences import DbPreferences


class UUIdGen:
    def __init__(self):
        self.default_icon = uuid4()
        self.mini_icon = uuid4()
        self.super_icon = uuid4()
        self.maxi_icon = uuid4()
        self.prof_icon = uuid4()


class Init(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Init, cls).__new__(cls)
            cls.sessions = get_sessions()
            cls.conf = Conf()
            cls.db = DataBase(cls.conf)
            cls.db_prefs = DbPreferences(cls.db)
            cls.flask_session = flask_session
        return cls.instance
