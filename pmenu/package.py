import subprocess


class AptListDiff:
    def __init__(self, old_list_app, new_list_app):
        self.new_apps = new_list_app.difference(old_list_app)
        self.removed_apps = old_list_app.difference(new_list_app)


class AptList:

    def __init__(self, apt_list_cache):
        self.apt_list_cache = apt_list_cache

    def write(self, new_list_app):
        with open(self.apt_list_cache, 'w') as f:
            f.write('\n'.join(new_list_app))

    def diff(self, new_list_app):
        old_list_app = []
        with open(self.apt_list_cache, 'r') as f:
            old_list_app = set(f.read().split('\n'))
        return AptListDiff(old_list_app, new_list_app)


def _pop(args):
    proc = subprocess.Popen(
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True
    )
    return proc.communicate()


def apt_exist(app_name):
    result = _pop([f'apt-cache search --names-only "^{app_name}$"'])
    if len(result[0]) > 1:
        return True
    return False


def all_apt_installed():
    _result = _pop(['apt list --installed'])

    raw_debs_installed = str(_result[0]).split(']')
    debs_installed = []

    for raw_line in raw_debs_installed:
        if raw_line.find('/') == -1:
            continue
        raw_deb = raw_line[raw_line.find('\\n') + 2:raw_line.find('/')]
        if not raw_deb:
            continue
        debs_installed.append(raw_deb)

    return set(debs_installed)


def apt_installed(app_name):
    result = _pop([f'dpkg -s "{app_name}"'])
    if len(result[0]) > 1:
        return True
    return False


def apt_name_for_desktop(desktop_path):
    result = _pop([f'dpkg -S "{desktop_path}"'])
    stdout = str(result[0])
    return stdout[0:stdout.find(':')]


def desktops_path_for_app(app_name):
    result = _pop([f'dpkg -L "{app_name}"'])
    list_of_files = str(result[0]).split('\\n')
    return [
        _file
        for _file in list_of_files
        if _file.endswith('.desktop')
    ]


def apt_install(app_name):
    proc = subprocess.Popen(
        ['apt-get install -f %s' % app_name],
        shell=True
    )
    result = proc.communicate()
    print(result)


def apt_remove(app_name):
    _pop(['apt-get remove -f %s' % app_name])


def give_all():
    result = _pop(
        ["dpkg --get-selections | sed 's:install$::'"]
    )[0].decode("utf-8").replace('\t', '').split('\n')
    return result


def sync_to_apt(db, conf, force=False):
    apt_apps = give_all()
    pmenu_apps = list(db.get_all_apps())
    levels = [dict(level) for level in list(db.get_levels())]

    for level in levels:
        level['nb_installed_apps'] = level['nb_not_installed_apps'] = 0
    apps_not_installed = []

    for apt_app in apt_apps:
        for pmenu_app in pmenu_apps:
            if (
                pmenu_app['apt_name'] == apt_app
                and pmenu_app['is_apt_installed'] == 0
            ):
                apps_not_installed.append(pmenu_app)
                pmenu_app['is_apt_installed'] == 1

    for app in apps_not_installed:
        db.update_apt_app(app['id'])
        if force:
            db.update_installed_app(app['apt_name'])
            continue
        if db.count_not_installed(app['apt_name']) > 0:
            db.update_installed_app(app['apt_name'])
    db.db.commit()

    pmenu_apps = list(db.get_all_apps())

    for pmenu_app in pmenu_apps:
        for level in levels:
            _test_and_inc(level, pmenu_app, 'mini', 0)
            _test_and_inc(level, pmenu_app, 'mini', 1)

            _test_and_inc(level, pmenu_app, 'super', 0)
            _test_and_inc(level, pmenu_app, 'super', 1)

            _test_and_inc(level, pmenu_app, 'maxi', 0)
            _test_and_inc(level, pmenu_app, 'maxi', 1)

            _test_and_inc(level, pmenu_app, 'prof', 0)
            _test_and_inc(level, pmenu_app, 'prof', 1)

    for level in levels:
        # update
        db.update_level(
            level['id'],
            level['nb_installed_apps'],
            level['nb_not_installed_apps']
        )
    db.db.commit()

    conf.first_launch = False
    conf.update()


def _test_and_inc(level, pmenu_app, session, level_nb):
    return
    level_name_suffix = 'level_name'
    if level == 1:
        level_name_suffix = 'sub_level_name'
    level_name = '%s_%s' % (session, level_name_suffix)
    if (
        level['session'] == session and level['level'] == level_nb
        and pmenu_app[level_name] == level['name']
    ):
        if (pmenu_app['is_available'] == 1):
            level['nb_installed_apps'] += 1
            return
        level['nb_not_installed_apps'] += 1
