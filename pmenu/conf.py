import configparser
from os.path import join, exists

from .debug import is_debug_mode


class DbConf:
    path = 'primtuxmenu.db'
    drop_path = join('SQL', 'DROP.sql')
    create_path = join('SQL', 'CREATE.sql')
    insert_apps_path = join('SQL', 'INSERT_apps.sql')
    insert_apps_detail_path = join('SQL', 'INSERT_apps_detail.sql')
    insert_levels_path = join('SQL', 'INSERT_levels.sql')
    insert_preferences_path = join('SQL', 'INSERT_preferences.sql')
    update_path = join('SQL', 'UPDATE.sql')

    def __init__(self, debug=False):
        if debug:
            return

        self.path = f'/usr/share/primtuxmenu/{self.path}'


class Conf:
    sessions = ['super', 'mini', 'maxi', 'prof']
    dynamic_conf = 'primtuxmenu.conf'
    first_launch = True
    last_app = None
    results_by_page = 50
    apt_list_cache = 'apt.txt'

    licenses = [
        'inconnu',
        'GPL 2',
        'GPL 3',
        'BSD',
        'Propriétaire',
        'Non défini'
    ]

    def get(self):
        config = configparser.RawConfigParser()
        config.read(self.dynamic_conf)
        if config.getboolean('DEFAULT', 'firstLaunch'):
            self.first_launch = config.get('DEFAULT', 'firstLaunch')
        if config.has_option('DEFAULT', 'lastApp'):
            self.last_app = config.get('DEFAULT', 'lastApp')

    def update(self):
        config = configparser.ConfigParser()
        config['DEFAULT'] = {
            'firstLaunch': self.first_launch
        }
        if self.last_app:
            config.set('DEFAULT', 'lastApp', self.last_app)
        with open(self.dynamic_conf, 'w') as configfile:
            config.write(configfile)

    def __init__(self, verbose=False):
        """Init Primtux store config"""
        self.verbose = verbose
        self.DEBUG = is_debug_mode()

        if not self.DEBUG:
            self.apt_list_cache = '/usr/share/primtuxmenu/apt.txt'
            self.dynamic_conf = '/etc/primtuxmenu/primtuxmenu.conf'

        self.assets = ''
        self.db = DbConf(self.DEBUG)
        if exists(self.dynamic_conf):
            self.get()
        else:
            self.update()
