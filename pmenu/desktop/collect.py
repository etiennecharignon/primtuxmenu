from glob import glob
from os import environ
from os.path import join, isdir

from .parse import PrimtuxDesktopFile
from .insert import InsertDesktops


class CollectDesktops:
    dirs = []
    debug = False

    def _get_dirs(self, xdg_data_dirs):
        dirs = []
        for path in xdg_data_dirs.split(':'):
            new_path = join(path, 'applications')
            if isdir(new_path):
                dirs.append(new_path)
        return dirs

    def _get(self):
        for path in self.dirs:
            for desktop_file in glob(f"{path}/*.desktop"):
                yield desktop_file

    def __init__(self, debug=False):
        self.debug = debug
        if debug:
            self.dirs = ['desktops']
            return
        self.dirs = self._get_dirs(environ.get('XDG_DATA_DIRS'))

    def collect(self):
        desktop_files = []
        for d in self._get():
            desktop_files.append(PrimtuxDesktopFile(d, self.debug))
        return InsertDesktops(desktop_files, self.debug)
