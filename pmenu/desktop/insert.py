from sqlite3 import IntegrityError

from pmenu.messages import error


class Menu:
    sql_id = 0
    session_name = ''
    level_one = ''
    level_two = None

    def __init__(self, session_name, level_one, level_two=None, _id=0):
        self.sql_id = _id
        self.session_name = session_name.lower()
        self.level_one = level_one.lower()
        if not level_two or level_two == '':
            self.level_two = None
            return
        self.level_two = level_two.lower()

    def __hash__(self):
        return hash((
            self.session_name,
            self.level_one,
            self.level_two
        ))

    def __str__(self):
        return f'{self.session_name}>{self.level_one}>{self.level_two}'


class Menus(set):
    hashes = {}

    def __init__(self, sql_levels=None):
        if not sql_levels:
            return

        for s in sql_levels:
            m = Menu(
                s['session'],
                s['name_level_one'],
                s['name_level_two'],
                s['id']
            )
            self.add(m)
            self.hashes[hash(m)] = m

    def populate(self, session):
        for _level in session.levels:
            for sb in _level.levels:
                m = Menu(
                    session.session,
                    _level.name,
                    sb.name
                )
                if hash(m) in self.hashes.keys():
                    continue
                self.add(m)
                self.hashes[hash(m)] = m

    def get(self, _hash):
        return self.hashes[_hash]


class InsertDesktops:
    desktop_files = []
    debug = False

    def _insert_app(self, desktop_file):
        in_mini = 0
        in_super = 0
        in_maxi = 0
        in_prof = 0
        is_available = 0
        is_web_app = 0
        is_external_url = 0

        if desktop_file.mini_session.levels != []:
            in_mini = 1
        if desktop_file.super_session.levels != []:
            in_super = 1
        if desktop_file.maxi_session.levels != []:
            in_maxi = 1
        if desktop_file.prof_session.levels != []:
            in_prof = 1

        if desktop_file.is_available:
            is_available = 1

        if desktop_file.is_web_app:
            is_web_app = 1

        if desktop_file.is_external_url:
            is_external_url = 1

        apt_name_label = 'apt_name,'
        apt_name_value = f"'{desktop_file.apt_name}',"
        if not desktop_file.apt_name:
            apt_name_label = ''
            apt_name_value = ''

        return f'''
            INSERT INTO "apps"
            (
                name,
                {apt_name_label}
                origin_desktop_name,
                default_icon_path,
                default_path,

                in_mini,
                in_super,
                in_maxi,
                in_prof,

                is_available,
                is_web_app,
                is_external_url
            )
            VALUES(
                "{desktop_file.default_name}",
                {apt_name_value}
                '{desktop_file.path}',
                '{desktop_file.icon}',
                '{desktop_file.exec}',

                {in_mini},
                {in_super},
                {in_maxi},
                {in_prof},

                {is_available},
                {is_web_app},
                {is_external_url}
            );
        '''

    def _insert_one_app_in_level(self, app_id, level_id):
        return f'''
            INSERT INTO "app_in_level"
            (
                app_key,
                level_key
            )
            VALUES (
                {app_id},
                {level_id}
            );
        '''

    def _insert_app_in_level(self, desktop_file, app_id, session):
        insert_list = []
        for _level in session.levels:
            if _level.levels == []:
                m = Menu(
                    session.session,
                    _level.name
                )
                try:
                    r = self.sql_menus.get(hash(m))
                except KeyError:
                    error(f"{desktop_file.path} : Le niveau '{_level.name}' n'existe pas")  # noqa: E501
                insert_list.append(self._insert_one_app_in_level(
                        app_id,
                        r.sql_id
                ))
            for sb in _level.levels:
                m = Menu(
                    session.session,
                    _level.name,
                    sb.name
                )
                try:
                    r = self.sql_menus.get(hash(m))
                except KeyError:
                    error(f"{desktop_file.path} > {_level.name}. Le sous-menu {sb.name} n'existe pas")  # noqa: E501
                insert_list.append(self._insert_one_app_in_level(
                        app_id,
                        r.sql_id
                ))

        return insert_list

    def __init__(self, desktop_files, debug=False):
        self.desktop_files = desktop_files
        self.debug = debug

    def _integrity(self, db, desktop_file):
        duplicate_sql = "SELECT origin_desktop_name, name, default_path FROM apps WHERE name = ? OR default_path = ?"  # noqa: E501
        duplicate_select = db.db.execute(
            duplicate_sql, [desktop_file.default_name, desktop_file.exec]
        ).fetchone()

        if not duplicate_select:
            error(f'Soucis d\'intégrité pour : "{desktop_file.path}" champ problématique non identifié (à corriger). Déjà existant dans : "{duplicate_select[0]}"')  # noqa: E501
            return

        if duplicate_select[1] == desktop_file.default_name:
            error(f'Soucis d\'intégrité pour : "{desktop_file.path}" pour le champ Name="{desktop_file.default_name}". Déjà existant dans : "{duplicate_select[0]}"')  # noqa: E501
            return

        if duplicate_select[2] == desktop_file.exec:
            error(f'Soucis d\'intégrité pour : "{desktop_file.path}" pour le champ Exec="{desktop_file.exec}". Déjà existant dans : "{duplicate_select[0]}"')  # noqa: E501
            return

        error(f'Soucis d\'intégrité pour : "{desktop_file.path}" champ problématique non identifié (à corriger). Déjà existant dans : "{duplicate_select[0]}"')  # noqa: E501

    def insert(self, db, verbose=False):
        self.sql_menus = Menus(db.get_levels())

        for desktop_file in self.desktop_files:
            app_sql = self._insert_app(desktop_file)

            try:
                insert_app = db.db.execute(app_sql)
            except IntegrityError:
                self._integrity(db, desktop_file)

            app_id = insert_app.lastrowid

            app_in_level_sql = self._insert_app_in_level(
                desktop_file,
                app_id,
                desktop_file.mini_session
            )
            app_in_level_sql += self._insert_app_in_level(
                desktop_file,
                app_id,
                desktop_file.super_session
            )
            app_in_level_sql += self._insert_app_in_level(
                desktop_file,
                app_id,
                desktop_file.maxi_session
            )
            app_in_level_sql += self._insert_app_in_level(
                desktop_file,
                app_id,
                desktop_file.prof_session
            )

            for sql in app_in_level_sql:
                db.db.execute(sql)

            db.db.commit()
