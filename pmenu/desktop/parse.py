from os.path import isfile, join, dirname, abspath
import sys
import configparser

from pmenu.messages import error, warning


class DesktopFile:
    path = ''
    default_name = None
    default_commment = None
    exec = None
    icon = None

    keywords = []

    i18n_names = {}
    i18n_comments = {}
    i18n_keywords = {}

    def _get_icon_path(self, icon_path):
        if self.debug:
            abs_dir = join(
                dirname(abspath(sys.argv[0])),
                'static',
                'assets',
                'apps',
            )
            if isfile(join(abs_dir, icon_path)):
                return icon_path
            icon_paths_with_ext = {
                f'{icon_path}.svg',
                f'{icon_path}.png',
                f'{icon_path}.jpg',
                f'{icon_path}.gif',
            }
            for icon_path_with_ext in icon_paths_with_ext:
                if isfile(join(abs_dir, icon_path_with_ext)):
                    return icon_path_with_ext
            return icon_path

    def __init__(self, path, debug):
        self.path = path
        self.debug = debug

        self.config = configparser.RawConfigParser(strict=debug)
        self.config.read(path)

        if 'Desktop Entry' not in self.config:
            raise Exception(
                'Desktop file malformed : no section "Desktop Entry"'
            )

        self.entry = self.config['Desktop Entry']

        if 'Name' in self.entry:
            self.default_name = self.entry['Name']
        if 'Comment' in self.entry:
            self.default_commment = self.entry['Comment']
        if 'Exec' in self.entry:
            self.exec = self.entry['Exec']
        if 'Icon' in self.entry:
            self.icon = self._get_icon_path(self.entry['Icon'])

    def __str__(self):
        return f'''
[Desktop Entry]
Name={self.default_name}
Comment={self.default_commment}
Exec={self.exec}'''


class PrimtuxLevel:
    name = ''
    levels = []

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name


class PrimtuxSession:
    is_empty = True
    session = ''
    levels = []

    def get_level(self, raw_levels):
        if not raw_levels:
            return []
        if ';' not in raw_levels:
            return [PrimtuxLevel(raw_levels.strip())]
        result = [
            PrimtuxLevel(level.strip())
            for level
            in raw_levels.split(';')
            if level.strip() != ''
        ]
        return result

    def __init__(self, entry, session_name):
        self.session = session_name.lower()

        if f'level[{self.session}]' not in entry:
            return

        self.is_empty = False

        self.levels = self.get_level(
            entry[f'level[{self.session}]']
        )

        for _key, _level in enumerate(self.levels):
            if f'level[{self.session}__{_level}]' in entry:
                self.levels[_key].levels = self.get_level(
                    entry[f'level[{self.session}__{_level}]']
                )


class PrimtuxDesktopFile(DesktopFile):
    apt_name = None
    mini_session = None
    super_session = None
    maxi_session = None
    prof_session = None
    is_available = False
    is_web_app = False
    is_external_url = False

    def _create_entry(self, session):
        session_name = session.session.lower()
        str_levels = ';'.join([_level.name for _level in session.levels])

        if session.levels == []:
            return ''

        for level in session.levels:
            entry = f'\n\nlevel[{session_name}]={str_levels}'
            if level.levels == []:
                continue
            str_sub_levels = ';'.join(
                [_level.name for _level in level.levels]
            )
            entry += f'level[{session_name}][{level.name}]={str_sub_levels}'
        return entry

    def __init__(self, path, debug):
        super(PrimtuxDesktopFile, self).__init__(path, debug)

        primtux_section = 'Primtuxmenu Levels'
        if primtux_section not in self.config:
            raise Exception(
                f'{primtux_section} malformed : no section "{primtux_section}"'
            )

        p_levels = self.config[primtux_section]

        if 'aptName' not in p_levels and 'externalUrl' not in p_levels:
            error(f'{path} doit avoir soit une entrée "aptName" soit "externalUrl".')  # noqa: E501

        if 'aptName' in p_levels and 'externalUrl' in p_levels:
            error(f'{path} ne peut pas avoir une entrée "aptName" et "externalUrl" en simultanné.')  # noqa: E501

        if 'aptName' in p_levels:
            self.apt_name = p_levels['aptName']

        # TODO: vrai vérification sur l'URL (elle existe ? 404 ?)
        if 'externalUrl' in p_levels and 'https' not in p_levels['externalUrl']:  # noqa: E501
            warning(f'{path} "externalUrl" contient une url en http (privilégier le https) : "{p_levels["externalUrl"]}"')  # noqa: E501

        if 'urlWebApp' in p_levels and 'externalUrl' in p_levels:
            warning(f'{path} entre "externalUrl" : "{p_levels["externalUrl"]}" et "urlWebApp" : "{p_levels["urlWebApp"]}", il faut choisir.')  # noqa: E501

        if 'externalUrl' in p_levels:
            self.is_available = True
            self.is_web_app = True
            self.is_external_url = True
            self.exec = p_levels['externalUrl']

        self.mini_session = PrimtuxSession(p_levels, 'Mini')
        self.maxi_session = PrimtuxSession(p_levels, 'Maxi')
        self.super_session = PrimtuxSession(p_levels, 'Super')
        self.prof_session = PrimtuxSession(p_levels, 'Prof')

        if (
            self.mini_session.is_empty
            and self.super_session.is_empty
            and self.maxi_session.is_empty
            and self.prof_session.is_empty
        ):
            error(f'{path} doit avoir une entrée de session !')

        if 'urlWebApp' in p_levels:
            self.is_web_app = True
            self.exec = p_levels['urlWebApp']

    def __str__(self):
        result = '\n[Primtuxmenu Levels]'

        result += self._create_entry(self.mini_session)
        result += self._create_entry(self.super_session)
        result += self._create_entry(self.maxi_session)
        result += self._create_entry(self.prof_session)
        result += f'\n\naptName={self.apt_name}'
        return result
