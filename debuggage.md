# Intro

L'objectif de cette page est de donner la liste des bonnes pratiques de débuggage du Primtuxmenu.
Attention, si vous sortez des clous de cette documentation, vous pouvez potentiellement altérer le fonctionnement du Primtuxmenu.
Cette documentation se veut par conséquent la plus exhaustive possible et l'objectif est la surveillance et l'identification et non la correction. (qui elle nécessite de retoucher les sources et proposer un update du paquet)

Le fonctionnement du **Primtuxmenu** est articulé autour du serveur `Nginx` et de services `systemd`.
L'avantage de systemd étant que le fonctionnement reste le même, peut importe le service.

```sh
systemctl enable|start|status|stop|restart|disable primtuxmenu
```

On distingue 3 services `systemd` différents :

1. `primtuxmenu` (lancé en espace admin) : le serveur web qui va afficher les différentes sessions et écrans disponibles.
C'est le coeur du projet qui concentre 90% du code, des fonctionnalités.
Il se lance en localhost sur http://127.0.0.1:5500 (http://127.0.0.1:1234 quand on est en développement)

2. `primtuxmenud` (lancé en espace admin) : c'est un service daemon. Ceci sous-entend qu'il n'affiche rien et tourne en tache de fond.
Il surveille actuellement les mises à jour du système.
Si l'utilisateur ajoute, supprime ou modifie des programmes (via `apt`, `synaptic` etc.), le service `primtuxmenud` en sera informé et mettra à jour la base de donnée en conséquence. Vu qu'il est couplé au `primtuxmenu`, il informera ce dernier afin de mettre à jour son apparence.
Ex : si l'utilisateur désinstalle une application et que le `primtuxmenu` l'affichait, l'écran se mettra à jour (sans intervention manuel) pour faire disparaitre cette application.

L'objectif de cette brique sera également de piloter l'installation d'applications à terme et donc de pouvoir aboutir à un store.

Cette brique, même si elle comporte peu de code, est assez délicate car les bugs sont difficiles à identifier et reproduires.

3. `primtuxmenuuser` (lancé en espace utilisateur : donc pour chaque session) : cette brique a pour seul vocation de lancer les applications natives (non web) avec les droits utilisateurs. C'est un simple client qui est entièrement piloté par les ordres du `primtuxmenu`.


Les 3 services sont mis à **enable** par défaut, ce qui sous-entend qu'il sera lancé automatiquement après chaque redémarrage.
Il est paramètré également avec l'option **Restart=always** : peut importe les messages d'erreurs rencontrés, le serveur rédémarrera.


## Le serveur `Nginx` :

Le serveur `nginx` est lancé automatiquement par le paquet debian lors de l'installation ou la mise à jour.
La config est situé dans `/etc/nginx/sites-available/primtux` :
- `primtuxmenu.conf` pour le serveur principal
- `primtuxmenud.conf` pour le daemon primtux

Nginx fait office de serveur proxy çàd qu'il va être en amont des requètes clientes.
Il va distinguer :
- la partie `statique` du site : favicon, images, css, javascript etc.
- la partie `dynamique` : serveur python gunicorn.

Un serveur web tel que `Nginx` a plusieurs rôles :
- Eviter certains soucis de sécurité en amont : attaque DDoS, man in the middle etc.
- Délivrer le contenu avec les meilleurs performances et une consommation réduite.

## Le service `primtuxmenu` :

La brique qui lance le serveur web.

1. Vérifier que le service fonctionne bien :

```sh
systemctl status primtuxmenu
```

2. Accéder aux logs :

```sh
journalctl -u primtuxmenu -b --no-pager
```

L'option **b** permet de restreindre aux entrées qui ont été recueillies depuis le dernier démarrage.

## Le service daemon `primtuxmenud` :

La brique qui lance le daemon de surveillance du primtuxmenu.

1. Vérifier que le service fonctionne bien :

```sh
systemctl status primtuxmenud
```

2. Accéder aux logs :

```sh
journalctl -u primtuxmenud -b --no-pager
```

L'option **b** permet de restreindre aux entrées qui ont été recueillies depuis le dernier démarrage.

## Le service (côté utilisateur) `primtuxmenuuser`:`

La brique qui lance les applications natives (non web) en espace utilisateur.

1. Vérifier que le service fonctionne bien :

```sh
systemctl --user status primtuxmenuuser
```

2. Accéder aux logs :

```sh
journalctl --user -u primtuxmenuuser -b --no-pager
```

L'option **b** permet de restreindre aux entrées qui ont été recueillies depuis le dernier démarrage.

## Utilisation avancée ?

Lorsqu'on modifie le contenu d'un service du primtuxmenu, il faut mettre à jour systemd :

```sh
 sudo systemctl daemon-reload
```

## Des soucis ?

Si vous êtes là, c'est que vous n'avez sans doute pas complètement suivi les recommandations faites en introduction.
Dans la plupart des cas, ce n'est pas bien grave.

Vous pouvez dans ce cas réinstaller proprement votre application Primtuxmenu via :

```sh
sudo apt remove primtuxmenu # l'option --purge ne change rien (pour l'instant)
sudo apt install primtuxmenu
```

Je ne propose pas la réinstallation car, si vous avez modifié (intentionnellement ou non) des fichiers de conf nginx ou systemd, il faut s'assurer de bien répondre aux questions les concernant.
