"use strict";

const c_modal_mask_el = getClassEl('c-modal__mask');

const C_MODAL_MASK_APPLY_CLASS = 'c-modal__mask--apply';
const C_MODAL_HIDDEN_CLASS = 'c-modal__container--hidden';

let c__modal = {
    is_open: false,
    close_callback: null,
    init: function() {
        this.is_open = false;
        this.close_callback = null;
    }
}

function c_modal__open(close_callback=null) {
    show_hide(c_modal_mask_el, false, C_MODAL_MASK_APPLY_CLASS);
    show_hide(COMPONENT_MODAL_CONTAINER, true, C_MODAL_HIDDEN_CLASS);
    c__modal.is_open = true;
    c__modal.close_callback = close_callback;
}

function c_modal__close() {
    show_hide(c_modal_mask_el, true, C_MODAL_MASK_APPLY_CLASS);
    show_hide(COMPONENT_MODAL_CONTAINER, false, C_MODAL_HIDDEN_CLASS);
    if (c__modal.close_callback) {
        window[c__modal.close_callback]();
    }
    c__modal.init();
}

document.onkeydown = function (e) {
    if (e.key == 'Escape')
        c_modal__close();
};
