function queries(params) {
    return Object.keys(params)
        .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        .join('&');
}

function filter_form(session_name) {
    let form = document.getElementById('edit-app-form');
    let result = {
        'session_name': session_name,
        'session_selected': get_session(session_name),
        'filter_by': get_filter_by(session_name)
    };
    form.querySelectorAll('input, textarea, select').forEach((el) => {
        if (el.getAttribute('type') == 'file')
            return;
        let init = el.getAttribute('value');
        let value = el.value;
        if (el.getAttribute('type') == 'checkbox')
            value = 0
            if (el.checked)
                value = 1
        if (el.getAttribute('data-force') == '1' && value) {
            result[el.getAttribute('name')] = value;
            return;
        }
        if (init == value)
            return;
        if (!init && !value)
            return;
        result[el.getAttribute('name')] = value;
    });
    return result;
}

function insert_app(session_name) {
    fetch(
        'insert-app',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(filter_form(session_name))
        }
    ).then(response => {
        response.json().then(value => {
            refresh_interface(value);
            c_modal__close();
        });
    });
}

function update_app(session_name) {
    fetch(
        'update-app',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(filter_form(session_name))
        }
    ).then(response => {
        response.json().then(value => {
            refresh_interface(value);
            c_modal__close();
        });
    });
}

function app_in_session(el, session) {
    show_hide(
        document.getElementById(session  + '-fieldset'),
        el.checked
    );
}

function edit_mode(el) {
    show_hide(
        getClassEl("c-new-app-button"),
        el.checked,
        "c-new-app-button--disabled"
    )
    fetch(
        'admin/edition-mode',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ 'edit': el.checked })
        }
    );
}

function restrict_apt(el) {
    el.value = el.value.replace(new RegExp(el.pattern), '');
}

function show_hides_confirm_delete() {
    show_hide(edit_action_el);
    show_hide(confirm_action_delete_el);
    show_hide(edit_app_form_el);
    show_hide(confirm_delete_app_el);
}

function ask_delete_app() {
    show_hides_confirm_delete();
}

function undo_delete_app() {
    show_hides_confirm_delete();
}

function delete_app(session_name, id, app_key) {
    let data = filter_form(session_name);
    data['id'] = id;
    if (app_key)
        data['app_key'] = app_key;
    fetch(
        'delete-app',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
    ).then(response => {
        response.json().then(value => {
            refresh_interface(value);
            c_modal__close();
        });
    });
}

function flush_tmp_files() {
    fetch(
        'flush-tmp-files',
        {
            method: 'POST'
        }
    );
}
