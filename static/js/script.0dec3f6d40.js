"use strict";

const body_el = document.body;
const edit_checkbox_el = document.getElementById('edit-mode-checkbox');
const edit_form_el = document.getElementById('edit-form');
const search_menu_el = document.getElementById('vertical-menu');

const COMPONENT_APPS_CONTAINER = getClassEl('c-app__container');
const COMPONENT_PAGINATION_CONTAINER = getClassEl('c-pagination__container');
const COMPONENT_MODAL_CONTAINER = getClassEl('c-modal__container');

const COMPONENT_MODAL_DECORATION = getClassEl('c-modal__decoration');
const COMPONENT_MODAL_BODY = getClassEl('c-modal__body');
const COMPONENT_MODAL_FOOTER = getClassEl('c-modal__footer');

let edit_action_el = null;
let confirm_action_delete_el = null;
let edit_app_form_el = null;
let confirm_delete_app_el= null;

function url_with_queries(url, params) {
    let query = Object.keys(params)
        .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        .join('&');
    return url + '?' + query;
}

function refresh_interface(json) {
    COMPONENT_APPS_CONTAINER.innerHTML = json['apps'];
    COMPONENT_PAGINATION_CONTAINER.innerHTML = json['pagination'];
}

function _refresh_url(params, session_name) {
    let location_params = Object.keys(params)
    .filter((item) => {
        return item != 'session_name' && item != 'session_selected' }
    )
    .reduce((object, key) => {
        return Object.assign(object, {
          [key]: params[key]
        });
    }, {});
    window.history.replaceState(
        null,
        document.title,
        url_with_queries(session_name, location_params)
    );
}

function go_level(
    page_index,
    level_name,
    sub_level_name,
    session_name='prof',
    filter_by=0
) {
    // store on localstorage :
    localStorage.setItem('page_index', page_index);
    localStorage.setItem('level_name', level_name);
    localStorage.setItem('sub_level_name', sub_level_name);
    localStorage.setItem('session_name', session_name);

    let all_notion_el = document.getElementById('select-menu__breadcrum-all-notions');
    let breadcrum_el = document.getElementById('select-menu__breadcrum');
    let breadcrum_level_el = document.getElementById('select-menu__breadcrum-level');
    let breadcrum_sub_level_el = document.getElementById('select-menu__breadcrum-sub-level');

    if (all_notion_el)
        show_hide(all_notion_el, true);
    if (breadcrum_el)
        show_hide(breadcrum_el, true);

    if (breadcrum_level_el) {
        breadcrum_level_el.classList.add('hidden');
        breadcrum_sub_level_el.classList.add('hidden');
        breadcrum_level_el.innerText = level_name;
    }

    if (breadcrum_level_el)
        show_hide(breadcrum_level_el, true, 'has-submenu');

    let params = {
        'session_name': session_name,
        'session_selected': get_session(session_name),
        'page_index': page_index,
        'filter_by': get_filter_by(session_name)
    };
    if (level_name) {
        params['level_name'] = level_name;
        if (all_notion_el)
            show_hide(all_notion_el, false);
        if (breadcrum_level_el)
            show_hide(breadcrum_level_el, true);
    }
    if (sub_level_name) {
        if (breadcrum_level_el)
            show_hide(breadcrum_level_el, false, 'has-submenu');
        params['sub_level_name'] = sub_level_name;
    }
    if (breadcrum_sub_level_el)
        breadcrum_sub_level_el.innerText = sub_level_name;
    if (sub_level_name && breadcrum_sub_level_el)
        show_hide(breadcrum_sub_level_el, true);

    _refresh_url(params, session_name);
    fetch(url_with_queries('apps', params)).then(response => {
        response.json().then(value => {
            refresh_interface(value);
        });
    });
}

function get_menu(session_name, session_selected=null) {
    let params = {
        'session_name': session_name,
        'session_selected': get_session(session_name, session_selected),
        'filter_by': get_filter_by(session_name)
    };
    fetch(url_with_queries('menu', params)).then(response => {
        response.json().then(value => {
            search_menu_el.innerHTML = value['menu'];
            refresh_interface(value);
        });
    });
}

function get_filter_by(session_name) {
    if (session_name != 'prof')
        return 0;
    let filter_el = document.querySelector('input[name="filter"]:checked');
    if (filter_el.id === 'edit-show-not-installed')
        return 1;
    if (filter_el.id === 'edit-show-both')
        return 2;
    return 0;
}

function get_session(session_name, session_selected=null) {
    if (!session_selected) {
        session_selected = session_name;
        let session_el = document.querySelector('input[name="session-selected"]:checked');
        if (session_el)
            session_selected = session_el.nextElementSibling.innerText.trim();
    }
    return session_selected;
}

function getClassEl(class_name) {
    return document.getElementsByClassName(class_name)[0];
}

function show_hide(component, value_exist, class_name=null) {
    if (!component) {
        console.warn("this element doesn't exist");
        return;
    }
    if (!class_name)
        class_name = 'hidden';
    if (value_exist) {
        component.classList.remove(class_name);
        return;
    }
    if (component.classList.contains(class_name)) {
        component.classList.remove(class_name);
        return;
    }
    component.classList.add(class_name);
}

function populate_app_detail(value, edit_mode) {
    COMPONENT_MODAL_DECORATION.innerHTML = value['modal_deco'];
    COMPONENT_MODAL_BODY.innerHTML = value['modal_body'];

    let modal_footer = value['modal_footer'];
    COMPONENT_MODAL_FOOTER.innerHTML = modal_footer;
    if (modal_footer)
        COMPONENT_MODAL_FOOTER.classList.add('c-modal__footer--decoration');
    else
        COMPONENT_MODAL_FOOTER.classList.remove('c-modal__footer--decoration');
    if (edit_mode)
        c_modal__open('flush_tmp_files');
    else
        c_modal__open();
}

let wait_details = false;

function show_details(session, id) {
    if (c__modal.is_open || wait_details)
        return;
    let edit_mode = false;
    if (document.querySelector('input[name="edit"]:checked'))
        edit_mode = true;
    let params = {
        'session': session,
        'edit_mode': edit_mode
    };
    wait_details = true;
    fetch(url_with_queries('app/' + id, params)).then(response => {
        response.json().then(value => {
            wait_details = false;
            if(!response.ok)
                return;
            populate_app_detail(value, edit_mode);
            edit_action_el = document.getElementById('edit-actions');
            confirm_action_delete_el = document.getElementById('action-confirm-delete');
            edit_app_form_el = document.getElementById('edit-app-form');
            confirm_delete_app_el = document.getElementById('confirm-delete-app');
        });
    });
}

function launch_app(session_name, id) {
    if (session_name == 'prof' && document.querySelector('input[name="edit"]:checked'))
        return;
    let params = {
        'session': session_name
    };
    fetch(url_with_queries('launch/' + id, params)).then(response => {
        response.text().then(value => {
            console.log(value);
        });
    });
}

function launch_web_app(web_app_url) {
    window.location.href = web_app_url;
}

function edit(el) {
    show_hide(edit_form_el, el.checked);
}

function create_app(el, session_name) {
    if (el.classList.contains('c-new-app-button--disabled'))
        return;
    if (c__modal.is_open || wait_details)
        return;
    let params = {
        'session_name': session_name
    };
    fetch(url_with_queries('create-app', params)).then(response => {
        response.json().then(value => {
            populate_app_detail(value);
        });
    });
}

function get_theme_key(session_type) {
    return 'theme_' + session_type;
}

function toggle_theme(session_type) {
    const html = document.getElementsByTagName('html')[0];
    const existing_theme = html.dataset.theme;
    const new_theme = existing_theme == 'dark' ? 'light' : 'dark';
    html.setAttribute('data-theme', new_theme);
    localStorage.setItem(get_theme_key(session_type), new_theme);
}

function init_theme(session_type) {
    const theme = localStorage.getItem(get_theme_key(session_type));
    if(theme) {
        const html = document.getElementsByTagName('html')[0];
        html.setAttribute('data-theme', theme);
    }
}

function open_session(session) {
    window.location.href = session;
}