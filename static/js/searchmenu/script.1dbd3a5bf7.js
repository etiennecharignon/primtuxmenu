"use strict";

const SEARCHMENU_FOCUS_EL = document.getElementById('focus-menu');

const scrollIntoViewHorizontally = (
    container,
    child,
  ) => {
    const child_offsetRight = child.offsetLeft + child.offsetWidth;
    const container_scrollRight = container.scrollLeft + container.offsetWidth;

    if (container.scrollLeft > child.offsetLeft) {
      container.scrollLeft = child.offsetLeft;
    } else if (container_scrollRight < child_offsetRight) {
      container.scrollLeft += child_offsetRight - container_scrollRight;
    }
  };

function c_menu_show_sub_menu(el) {
    const submenu = document.getElementById('submenu_' + el.id);
    if (submenu) {
        setTimeout(() => {
            submenu.classList.remove('hidden');
            setTimeout(() => {
                submenu.classList.add('horizontal-sub-menu--show');
                const input = submenu.querySelector("input");
                input.checked = true;
                input.dispatchEvent(new Event('change'));

            });
        });
        c_menu_manage_scroll_button_visibility(submenu.querySelector('.horizontal-menu__scroll-container'));
    }
}

function c_menu_hide_sub_menu(selected_level_el) {
    const submenu = document.getElementById('submenu_' + selected_level_el.id);
    if (submenu) {
        submenu.classList.remove('horizontal-sub-menu--show');
        submenu.classList.add('hidden');
        setTimeout(() => { submenu.classList.add('hidden'); });
    }
}

function c_menu_manage_scroll_button_visibility(scrollContainer) {
    const leftButton = scrollContainer.querySelector(".horizontal-menu__scroll-button--left");
    if (scrollContainer.scrollLeft <= 0) {
        leftButton.classList.add('hidden');
    }
    else {
        leftButton.classList.remove('hidden');
    }
    const rightButton = scrollContainer.querySelector(".horizontal-menu__scroll-button--right");
    if (Math.floor(scrollContainer.scrollLeft - scrollContainer.scrollWidth + scrollContainer.clientWidth) >= 0) {
        rightButton.classList.add('hidden');
    }
    else {
        rightButton.classList.remove('hidden');
    }

    if(scrollContainer.scrollWidth > scrollContainer.clientWidth) {
        scrollContainer.querySelector('.select-menu').classList.remove('select-menu--row-centered');
    }
    else {
        scrollContainer.querySelector('.select-menu').classList.add('select-menu--row-centered');
    }
}

function c_menu_scroll(scrollContainer, get_scroll_step) {
    scrollContainer.scrollLeft += get_scroll_step();
    c_menu_manage_scroll_button_visibility(scrollContainer);
}

function c_menu_init() {
    const scrollContainers = document.querySelectorAll(".horizontal-menu__scroll-container");

    scrollContainers.forEach((scrollContainer) => {
        function compute_scroll_step(direction) {
            const itemsList = scrollContainer.querySelector('.select-menu');
            return direction * scrollContainer.scrollWidth / (itemsList.childElementCount / 2);
        }

        c_menu_manage_scroll_button_visibility(scrollContainer);
        scrollContainer
            .querySelector(".horizontal-menu__scroll-button--right")
            .addEventListener('click', () => { c_menu_scroll(scrollContainer, ()=> compute_scroll_step(1)); })
        scrollContainer
            .querySelector(".horizontal-menu__scroll-button--left")
            .addEventListener('click', () => { c_menu_scroll(scrollContainer, () => compute_scroll_step(-1)); })
        scrollContainer.addEventListener("wheel", (event) => {
            c_menu_manage_scroll_button_visibility(scrollContainer);

            if(event.deltaY != 0 && event.deltaX == 0) {
                event.preventDefault();
                c_menu_scroll(scrollContainer, () => event.deltaY);
            }
        })
    });

    document.querySelectorAll(".horizontal-menu__scroll-container").forEach((scrollContainer) => {
        document.querySelectorAll("input").forEach((input) => {
            input.addEventListener("change", () => {
                scrollIntoViewHorizontally(scrollContainer, input.nextElementSibling);
            });
        });
    });

    const inputs = document.querySelectorAll(".select-menu__input");
    inputs.forEach((input) => {
        input.addEventListener("change", () => {
            inputs.forEach((input) => {
                c_menu_hide_sub_menu(input)
            });
            c_menu_show_sub_menu(input);
        });
    });

    document.querySelectorAll(".app__input").forEach((input) => {
        input.addEventListener("change", input.blur);
    });
}
