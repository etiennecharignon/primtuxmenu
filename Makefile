.ONESHELL:

.PHONY: unittest ruff flake8 requirements build init run.server run.daemon prod db.create db.dump db.record test diagram clean tmux cypress.open deb.replace build.debian

SHELL := /bin/bash

unittest:
	source ./bin/activate
	python3 -m unittest tests/*.py

ruff:
	source ./bin/activate
	ruff check .

flake8:
	source ./bin/activate
	flake8 --filename="./pmenu/**,./tests/**,./primtuxmenu.py,.build,./clean,./update_debs,./install.py,./create_db,./dump,./sync_db,./primtuxmenu_daemon.py"
	make ruff

requirements:
	source ./bin/activate
	pip install -r requirements.txt

build:
	source ./bin/activate
	python ./build

init:
	python3 -m venv .
	make requirements
	make db.init
	make build

run.server:
	source ./bin/activate
	FLASK_APP=primtuxmenu.py FLASK_ENV=development flask run -p 1234

run.daemon:
	source ./bin/activate
	FLASK_APP=primtuxmenu_daemon FLASK_ENV=development flask run -p 8000

db.create:
	source ./bin/activate
	python ./create_db --verbose

db.dump:
	source ./bin/activate
	python ./dump

db.record:
	make db.dump
	git add SQL && git add static

db.sync:
	source ./bin/activate
	python ./sync_db --dev

db.init:
	make db.create && make db.sync

db.recover:
	git pull
	make db.init

test:
	source ./bin/activate
	make unittest
	make flake8

diagram:
	dot -Tpng diagram.dot -o diagram.png
	dot -Tsvg diagram.dot -o diagram.svg

clean:
	source ./bin/activate
	rm -f MANIFEST
	rm -rf build dist
	./clean

tmux:
	tmuxp load tmux.yml

cypress.open:
	./node_modules/.bin/cypress open

deb.replace:
	source ./bin/activate
	./update_debs --path $(path)

build.debian:
	make db.create
	make build
	debuild -us -uc
	make db.sync
