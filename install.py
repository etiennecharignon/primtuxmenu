import subprocess
import secrets

from flask import Flask
from flask_socketio import SocketIO, send

from pmenu.conf import Conf
from pmenu.request import DataBase
# from pmenu.package import (
#    apt_install, apt_installed, apt_exist, apt_remove, sync_to_apt
# )


app = Flask(__name__)

app.secret_key = secrets.token_urlsafe(32)
socketio = SocketIO(app, cors_allowed_origins="*")

conf = Conf()


@socketio.on('message')
def handle_message(data):
    print('received message: ' + data)


@socketio.on('install', namespace='/ws/install')
async def ws_install(data):
    db = DataBase(conf)
    app = db.get(data)
    # TODO : --dry-run on dev mode
    with subprocess.Popen(
        ['apt-get', 'install', '%s' % app['apt_name']],
        stdout=subprocess.PIPE,
        bufsize=1,
        universal_newlines=True
    ) as process:
        for line in process.stdout:
            line = line.rstrip()
            await send(line)


@socketio.on('/ws/uninstall')
async def ws_uninstall(data):
    db = DataBase(conf)
    app = db.get(data)
    # TODO : --dry-run on dev mode
    with subprocess.Popen(
        ['apt-get', 'remove', '-f', '%s' % app['apt_name']],
        stdout=subprocess.PIPE,
        bufsize=1,
        universal_newlines=True
    ) as process:
        for line in process.stdout:
            line = line.rstrip()
            await send(line)
        await send('end')


if __name__ == '__main__':
    socketio.run(app, port=7000)
