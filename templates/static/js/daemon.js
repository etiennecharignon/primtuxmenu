const socket = io('ws://127.0.0.1:8000');

socket.on("start", () => {
    socket.send({'first': true});
});

socket.on("watch", (arg) => {
    path = location.pathname.substring(1);

    if (!['mini', 'super', 'maxi', 'prof'].includes(path))
        return;

    go_level(
        localStorage.getItem('page_index'),
        localStorage.getItem('level_name'),
        localStorage.getItem('sub_level_name'),
        localStorage.getItem('session_name')
    );
});
