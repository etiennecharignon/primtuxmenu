# Primtuxmenu

## Présentation

Le **Primtuxmenu** est le nouveau remplaçant des **Handymenus**.
Le but de ce projet est d'avoir un environnement homogène, moderne et agréable en adéquation avec le programme scolaires des 3 premiers cycles.
Afin de simplifier au maximum l'expérience utilisateur, l'objectif est également d'avoir un outil qui simplifie l'installation, le déploiement et la configuration de **Primtux**.

## Un peu d'histoire

Depuis le début de **Primtux**, les sessions disposent d'un lanceur pour leurs applications : les **Handymenus**.
Un second lanceur a fait son entrée : **BNE** pour la version [dys](https://fr.wikipedia.org/wiki/Troubles_dys).
Chacun présente ses avantages : le **BNE** est graphiquement plus abouti et les **Handymenus** plus configurables.

Parralèlement à ça, l'équipe Primtux a créé plusieurs applications éducatives en full web et c'est orienté vers la création d'un store.
Il y a aussi eu une remise en question de l'utilité actuel de Primtux : par exemple, comment fournir des services à des écoles qui ont du matériel hétérogène :
un mix windows + tablettes.

Le **Primtuxmenu** est la contraction de toutes ces évolutions :

- Un lanceur adapté aux différents cycles et en adéquation avec le programme scolaire. (hierarchisé en ce sens)
- Un store pour le prof (l'eRUN, l'admin, le parent etc) qui lui permet d'installer/supprimer des applications à son gré.
- La possibilité d'utiliser **Primtux** comme serveur pour délivrer des applications webs adaptés (responsive) à des périphériques tel que des tablettes, ordis sous windows etc.
  le web est le seul dénominateur commun qui fonctionne partout.
- à terme, d'autres évolutions vont pouvoir émerger tel que (liste non exhaustive) :
    * Des graphiques d'utilisation.
    * Des outils d'évaluation.
    * De l'inter-activité entre professeurs et élèves voir entre élèves.

## Sous le capot (Dev)

Vu que le **PrimtuxMenu** part d'une page blanche, le but est d'être le plus novateur possible tout en donnant les ressources à tout à chacun de participer. (ce qui s'apparente, je le concède à résoudre le problème de la quadrature du cercle)

- Le 1er axe est le web avec comme première directive de ne pas utiliser de framework javascript : les 3 grands frameworks actuelles (React, VueJS, Angular) ont tous leurs avantages et inconvénients mais on tendance à fractionner les devs.
Dans la même veine, éviter au maximum les dépendances à des libs.

- Le 2ème axe (qui prolonge le 1er) : Primtuxmenu doit pouvoir vivre longtemps sans intervention d'un développeur :
L'équipe de **Primtux** est réduite et composé de bénévoles... il peut y avoir des indisponibilités plus ou moins grandes et celà ne doit pas affecter l'expérience utilisateur.

- le serveur est écris en **Python** et utilise le framework Flask 2 (asynchronisme natif, HTTP2 et websocket) pour diverses raisons :
langage simple à appréhender et populaire, pas de compilation donc un .deb pour l'ensemble de nos archis (i386, arm, amd 64).

- la partie front se base sur un **Design System** :
L'objectif étant d'avoir des composants réutilisables et une vrai trame de nos modes opératoires.

- BDD SQL avec **SQLite** : simple et efficace

- des tests et de l'intégration continue : la solidité d'une application, c'est important. Nous mettons tout en oeuvre pour garantir la qualité.

## Dev ++ : venez participer

Le projet peut être un bon terreau pour acquérir des connaissances, monter en compétence, participer à un projet communautaire qui a un sens éthique etc.
Votre profil peut tout à fait être :
- orienté graphisme
- orienté dev front
- orienté dev back
- un mélange de tout ça

Vous n'avez pas forcément besoin d'être dans l'enseignement ou un parent d'élève pour participer !
